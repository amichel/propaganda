const express = require('express')
const router = express.Router()

const home = require('./home.js')
const usuarios = require('./usuarios.js')
const productos = require('./productos.js')
const pagos = require('./pagos.js')

router.use('/',home)
router.use('/usuarios',usuarios)
router.use('/productos',productos)
router.use('/pagos',pagos)

module.exports = router